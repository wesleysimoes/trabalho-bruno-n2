import { PostPage } from './../post/post';
import { CommentPage } from './../comment/comment';
import { PhotoPage } from './../photo/photo';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AddpostPage } from '../addpost/addpost';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  
  Posts = PostPage;
  Comments = CommentPage;
  Photos = PhotoPage;
  AddPosts = AddpostPage;

  constructor(public navCtrl: NavController) {

  }


}
