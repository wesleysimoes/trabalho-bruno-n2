import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the PostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {

  posts: any;

  constructor(public navCtrl: NavController, public restProvider: RestProvider) {
    
    this.getPosts();
    
  }
  
  getPosts() {
    this.restProvider.getPosts()
    .then(data => {
      this.posts = data;
      console.log(this.posts);
    });
  }

}
