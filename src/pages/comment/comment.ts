import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the CommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
})
export class CommentPage {

  comments: any;

  constructor(public navCtrl: NavController, public restProvider: RestProvider) {
    
    this.getComments();

  }

  getComments() {
    this.restProvider.getComments()
    .then(data => {
      this.comments = data;
      console.log(this.comments);
    });
  }

}
