import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { PostPage } from '../pages/post/post';
import { CommentPage } from '../pages/comment/comment';
import { PhotoPage } from '../pages/photo/photo';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {


  @ViewChild(Nav) nav: Nav;
  pages1: {title: 'Home Page', component: HomePage};
  pages2: {title: 'Post Page', component: PostPage};
  pages3: {title: 'Comment Page', component: CommentPage};
  pages4: {title: 'Photo Page', component: PhotoPage};
  rootPage = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {


    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    }

  openPage(page: {title: string, component: any}): void{
    //this.rootPage = page.component;
    this.nav.setRoot(page.component);
  }
}

